//
//  BotViewController.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import UIKit
import AWSLex
import AWSCognitoIdentityProvider


struct ChatMessage {
    let text: String
    let isInComing: Bool
    let date: Date
    
}

extension UIView {
    
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
    
}

extension Date {
    static func dateFromCustomString(customString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: customString) ?? Date()
    }
}


class BotViewController: UIViewController,  UITextFieldDelegate, AWSLexInteractionDelegate {
    
    var user:AWSCognitoIdentityUser?
    var userAttributes:[AWSCognitoIdentityProviderAttributeType]?
    var mfaSettings:[AWSCognitoIdentityProviderMFAOptionType]?
    
    @IBOutlet weak var botButtonInputCollectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageInputContainer: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    fileprivate var incomingCell = "incomingCell"
    fileprivate var outgoingCell = "outgoingCell"
    
    var bottomConstraint: NSLayoutConstraint?
    var interactionKit: AWSLexInteractionKit?
    
    var textMessages: [ChatMessage] = []
    
    
    var botFeatures = ["Record","Track","History","Predict"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        fetchUserAttributes()
        
        tableView.register(BotMessageTableViewCell.self, forCellReuseIdentifier: incomingCell)
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(named: "lightgrey")
        tableView.allowsSelection = false
        
        // Pin at botton view
        inputTextField.delegate = self
        
        inputTextField.addTarget(self, action: #selector(BotViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        bottomConstraint = NSLayoutConstraint(item: messageInputContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 50)
        
        view.addConstraint(bottomConstraint!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name:
            UIResponder.keyboardWillShowNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        setUpLex()
    }
    
    @IBAction func backHome(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func BotInputAction(_ sender: UIButton) {
        
        if let title = sender.currentTitle {
            
            sendToLex(text: title)
            
        }
    }
    
    func addNavBarImage(){
        
        
        let navController = navigationController!
        
        let image = #imageLiteral(resourceName: "allergyBot")
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        imageView.frame = CGRect(x: 0, y: 0, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        imageView.center = navController.navigationBar.center
        
        navigationItem.titleView = imageView
        
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            print(keyboardFrame)
            
            let isKeyboardshowing = notification.name == UIResponder.keyboardWillShowNotification 
            
            bottomConstraint?.constant = isKeyboardshowing ? -keyboardFrame.height: 0
            
            
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                
                if isKeyboardshowing {
                    
                    let indexPath = NSIndexPath(item: self.textMessages.count - 1, section: 0)
                    self.tableView.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
                    
                }
                
            })
            
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !(textField.text?.isEmpty)! {
            sendButton.isEnabled = true
            sendButton.tintColor = UIColor(named: "coreBlue")
        }else{
            sendButton.tintColor = UIColor(named: "strongGrey")
        }
    }
    
    // MARK AWS Lex
    func setUpLex(){
        self.interactionKit = AWSLexInteractionKit.init(forKey: "chatConfig")
        self.interactionKit?.interactionDelegate = self
    }
    
    func interactionKit(_ interactionKit: AWSLexInteractionKit, onError error: Error) {
        print("interactionKit error: \(error)")
    }
    
    func sendToLex(text : String){
        self.interactionKit?.text(inTextOut: text)
        
        textMessages.append(ChatMessage(text: text, isInComing: false, date: Date()))
        
        let item = textMessages.count - 1
        let insertionIndexPath = NSIndexPath(item: item, section: 0) as IndexPath
        
        self.tableView.insertRows(at: [insertionIndexPath], with: .left)
        self.tableView.scrollToRow(at: insertionIndexPath, at: .bottom, animated: true)
        self.inputTextField.text = nil
        self.sendButton.tintColor = UIColor(named: "strongGrey")
    }
    
    //handle response -- callback
    func interactionKit(_ interactionKit: AWSLexInteractionKit, switchModeInput: AWSLexSwitchModeInput, completionSource: AWSTaskCompletionSource<AWSLexSwitchModeResponse>?) {
        guard let response = switchModeInput.outputText else {
            let response = "No reply from bot"
            print("Response: \(response)")
            return
        }
        //show response on screen
        DispatchQueue.main.async{
            self.textMessages.append(ChatMessage(text: response, isInComing: true, date: Date()))
            
            let item = self.textMessages.count - 1
            let insertionIndexPath = NSIndexPath(item: item, section: 0) as IndexPath
            
            self.tableView.insertRows(at: [insertionIndexPath], with: .left)
            self.tableView.scrollToRow(at: insertionIndexPath, at: .bottom, animated: true)
            self.inputTextField.text = nil
            self.sendButton.tintColor = UIColor(named: "strongGrey")
        }
    }
    
    @IBAction func handleSend(_ sender: UIButton) {
        
        if let text = inputTextField.text {
            if text.isEmpty {
                sendButton.isEnabled = false
            }else{
                
                //reuqest meesage from bot
                sendToLex(text: inputTextField.text!)
            }
        }
    }
    
    func displayAuthentication(){
        
    }
    
    func userAuthentication(){
        
    }
    
    
}
extension BotViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textMessages.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: incomingCell, for: indexPath) as! BotMessageTableViewCell
        
        let chatMessage = textMessages[indexPath.row]
        
        cell.chatMessage = chatMessage
        
        return cell
    }
    
    class DateHeaderLabel: UILabel {
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            backgroundColor = UIColor(named: "strongBlue")
            textColor = .white
            textAlignment = .center
            translatesAutoresizingMaskIntoConstraints = false
            font = UIFont.boldSystemFont(ofSize: 14)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override var intrinsicContentSize: CGSize {
            let originalContentSize = super.intrinsicContentSize
            let height = originalContentSize.height + 12
            layer.cornerRadius = 12
            layer.masksToBounds = true
            
            return CGSize(width: originalContentSize.width + 20, height: height)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateString = dateFormatter.string(from: Date())
        
        let label = DateHeaderLabel()
        label.text = dateString
        
        let containerView = UIView()
        containerView.addSubview(label)
        label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        return containerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func valueForAttribute(name:String) -> String? {
        let values = self.userAttributes?.filter { $0.name == name }
        return values?.first?.value
    }
    
    
    func fetchUserAttributes() {
        var name = ""
        
        user = AppDelegate.defaultUserPool().currentUser()
        user?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
            guard task.result != nil else {
                return nil
            }
            self.userAttributes = task.result?.userAttributes
            self.mfaSettings = task.result?.mfaOptions
            self.userAttributes?.forEach({ (attribute) in
                print("Name: " + attribute.name!)
            })
            
            name = self.valueForAttribute(name: "given_name")!
            DispatchQueue.main.async {
                
                self.customBotReply(message: "Welcome back!, Hi \(name), please select your action!.")
            }
            return nil
        })
    }
    
    func customBotReply(message:String){
        self.textMessages.append(ChatMessage(text: message, isInComing: true, date: Date()))
        
        let item = self.textMessages.count - 1
        let insertionIndexPath = NSIndexPath(item: item, section: 0) as IndexPath
        
        self.tableView.insertRows(at: [insertionIndexPath], with: .left)
        
    }
    
}

extension BotViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return botFeatures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subButtonInput", for: indexPath) as! BotSubInputButtonCollectionViewCell
        
        cell.displayContent(title: botFeatures[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("Record")
            self.interactionKit?.text(inTextOut: "Record", sessionAttributes: nil)
        case 1:
            self.interactionKit?.text(inTextOut: "Track", sessionAttributes: nil)
        case 2:
            self.interactionKit?.text(inTextOut: "Analysis", sessionAttributes: nil)
        case 3:
            self.interactionKit?.text(inTextOut: "History", sessionAttributes: nil)
        default:
            self.interactionKit?.text(inTextOut: "Hello", sessionAttributes: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
