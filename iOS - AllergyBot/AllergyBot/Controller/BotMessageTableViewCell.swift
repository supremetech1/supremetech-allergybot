//
//  BotMessageTableViewCell.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import UIKit

class BotMessageTableViewCell: UITableViewCell {
    
    let messageLabel = UILabel()
    let bubbleBackgroundView = UIView()
    
    var leadingConstarint: NSLayoutConstraint!
    var trailingConstarint: NSLayoutConstraint!
    
    var chatMessage: ChatMessage! {
        didSet {
            bubbleBackgroundView.backgroundColor = chatMessage.isInComing ? .white : UIColor(named: "coreBlue")
            messageLabel.textColor = chatMessage.isInComing ? .black : .white
            
            messageLabel.text = chatMessage.text
            
            if chatMessage.isInComing {
                leadingConstarint.isActive = true
                trailingConstarint.isActive = false
            }else {
                leadingConstarint.isActive = false
                trailingConstarint.isActive = true
            }
            
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        bubbleBackgroundView.layer.cornerRadius = 12
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints =  false
        addSubview(bubbleBackgroundView)
        
        addSubview(messageLabel)
        messageLabel.text = "We want to provide a longer string that is actually going to wrap onto the next line and maybe ven a theird line."
        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        //Constraints
        let constraints =
            [messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
             messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
             messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
             
             bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -12),
             bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -12),
             bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 12),
             bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 12),
             ]
        NSLayoutConstraint.activate(constraints)
        
        leadingConstarint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
        leadingConstarint.isActive = true
        
        trailingConstarint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        trailingConstarint.isActive = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

