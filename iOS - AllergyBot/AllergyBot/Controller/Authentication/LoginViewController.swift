//
//  LoginViewController.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import UIKit
import OnboardKit

import AWSCognitoIdentityProvider

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


class LoginViewController: UIViewController {
    
    @IBOutlet weak var passwordInput: UITextField!
    
    @IBOutlet weak var usernameInput: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    
    lazy var onboardingPages: [OnboardPage] = {
        let pageOne = OnboardPage(title: "Record your meal",
                                  imageName: "onBoardRecord",
                                  description: "Save your daily meal record, data will be persist in the cloud.")
        
        let pageTwo = OnboardPage(title: "Track your allergy",
                                  imageName: "onBoardTrack",
                                  description: " Track your allergy seamlessly by talking with the bot.")
        
        let pageThree = OnboardPage(title: "Let the bot make a prediction",
                                    imageName: "onBoardPredict",
                                    description: "With your previous record, get your allergy prediction from the bot.")
        
        return [pageOne, pageTwo, pageThree]
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        showOnboardingScreen()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if (self.usernameInput!.text!.isEmpty || self.passwordInput!.text!.isEmpty) {
            let alertController = UIAlertController(title: "Username/Pssword cannot be empty",
                                                    message: "Please enter your username or password",
                                                    preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        
        let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: self.usernameInput!.text!, password: self.passwordInput!.text! )
        
        self.passwordAuthenticationCompletion?.set(result: authDetails)
        
    }
    
    
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        if (self.usernameInput!.text!.isEmpty) {
            let alertController = UIAlertController(title: "Enter Username",
                                                    message: "Please enter your username and then select Forgot Password if you want to reset your password.",
                                                    preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion:  nil)
            return
        }
        self.performSegue(withIdentifier: "ForgotPasswordSegue", sender: self)
    }
    
    
    
    func inputDidChange(_ sender:AnyObject) {
        if (self.usernameInput?.text != nil && self.passwordInput?.text != nil) {
            self.loginButton?.isEnabled = true
        } else {
            self.loginButton?.isEnabled = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ForgotPasswordSegue" {
            let forgotPasswordController = segue.destination as! ForgotPasswordViewController
            forgotPasswordController.emailAddress = self.usernameInput!.text!
        }
    }
    
    func showOnboardingScreen() {
        
        let onboardingStatus = UserDefaults.standard.bool(forKey: "onboardingComplete")
        print("onboarding value \(onboardingStatus)")
        if(onboardingStatus){
            
        }else if(onboardingStatus == false) {
            let onboardingVC = OnboardViewController(pageItems: onboardingPages)
            onboardingVC.modalPresentationStyle = .formSheet
            onboardingVC.presentFrom(self, animated: true)
            
            newUserOnboarding()
        }
        
    }
    
    func newUserOnboarding() {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "onboardingComplete")
        userDefaults.synchronize()
        
    }
    
}

extension LoginViewController: AWSCognitoIdentityPasswordAuthentication {
    
    public func getDetails(_ authenticationInput: AWSCognitoIdentityPasswordAuthenticationInput, passwordAuthenticationCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>) {
        
        self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource
        DispatchQueue.main.async {
            if self.usernameInput.text!.isEmpty {
                self.usernameInput?.text = authenticationInput.lastKnownUsername
            }
        }
    }
    
    public func didCompleteStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            print(error?.localizedDescription)
            if error != nil {
                let alertController = UIAlertController(title: "Cannot Login",
                                                        message: (error! as NSError).userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
                alertController.addAction(retryAction)

                self.present(alertController, animated: true, completion:  nil)
            } else {
                self.dismiss(animated: true, completion: {
                    self.usernameInput?.text = nil
                    self.passwordInput?.text = nil
                })
            }
        }
    }
    
}
