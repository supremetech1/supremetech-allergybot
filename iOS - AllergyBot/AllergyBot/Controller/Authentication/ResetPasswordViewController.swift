//
//  ResetPasswordViewController.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import Foundation
import UIKit
import AWSCognitoIdentityProvider

class ResetPasswordViewController: UIViewController {
    
    
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var newPasswordInput: UITextField!
    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var lastNameInput: UITextField!
    
    var currentUserAttributes:[String:String]?
    
    var resetPasswordCompletion: AWSTaskCompletionSource<AWSCognitoIdentityNewPasswordRequiredDetails>?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.newPasswordInput.text = nil
        self.newPasswordInput.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        self.firstNameInput.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        self.lastNameInput.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //         Hide Keyboard after tap is done
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        if (self.newPasswordInput.text != nil && self.firstNameInput != nil && self.lastNameInput != nil) {
            self.newPasswordButton.isEnabled = true
        } else {
            self.newPasswordButton.isEnabled = false
        }
    }
    
    @IBAction func submitNewPassword(_ sender:AnyObject) {
        var userAttributes:[String:String] = [:]
        userAttributes["family_name"] = self.lastNameInput.text
        userAttributes["given_name"] = self.firstNameInput.text
        let details = AWSCognitoIdentityNewPasswordRequiredDetails(proposedPassword: self.newPasswordInput.text!, userAttributes: userAttributes)
        self.resetPasswordCompletion?.set(result: details)
    }
    
}

extension ResetPasswordViewController: AWSCognitoIdentityNewPasswordRequired {
    
    public func getNewPasswordDetails(_ newPasswordRequiredInput: AWSCognitoIdentityNewPasswordRequiredInput, newPasswordRequiredCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityNewPasswordRequiredDetails>) {
        self.currentUserAttributes = newPasswordRequiredInput.userAttributes
        self.resetPasswordCompletion = newPasswordRequiredCompletionSource
    }
    
    public func didCompleteNewPasswordStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            if let error = error as NSError? {
                let alertController = UIAlertController(title: error.userInfo["__type"] as? String,
                                                        message: error.userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
                alertController.addAction(retryAction)
                self.present(alertController, animated: true, completion:  nil)
            } else {
                self.newPasswordInput.text = nil
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
}

