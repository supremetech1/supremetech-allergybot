//
//  VerificationViewController.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class VerificationViewController: UIViewController {
    
    
    @IBOutlet weak var verificationField: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var verificationLabel: UILabel!
    
    var codeDeliveryDetails:AWSCognitoIdentityProviderCodeDeliveryDetailsType?
    
    var user: AWSCognitoIdentityUser?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.verifyButton.isEnabled = false
        self.verificationField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        populateCodeDeliveryDetails()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Hide Keyboard after tap is done
        self.hideKeyboardWhenTappedAround()
    }
    
    
    
    func populateCodeDeliveryDetails() {
        let isEmail = (codeDeliveryDetails?.deliveryMedium == AWSCognitoIdentityProviderDeliveryMediumType.email)
        verifyButton.setTitle(isEmail ? "Verify Email Address" : "Verify Phone Number", for: .normal)
        let medium = isEmail ? "your email address" : "your phone number"
        let destination = codeDeliveryDetails!.destination!
        verificationLabel.text = "Please enter the code that was sent to \(medium) at \(destination)"
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        if(verificationField.text == nil) {
            self.verifyButton.isEnabled = false
            return
        }
        self.verifyButton.isEnabled = true
    }
    
    func resetConfirmation(message:String? = "") {
        self.verificationField.text = ""
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: nil))
        self.present(alert, animated: true, completion:nil)
    }
    
    @IBAction func verifyPressed(_ sender: Any) {
        self.user?.confirmSignUp(verificationField.text!)
            .continueWith(block: { (response) -> Any? in
                if response.error != nil {
                    self.resetConfirmation(message: (response.error! as NSError).userInfo["message"] as? String)
                } else {
                    DispatchQueue.main.async {
                        // Return to Login View Controller - this should be handled a bit differently, but added in this manner for simplicity
                        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    }
                }
                return nil
            })
    }
    
    @IBAction func resendConfirmationCodePressed(_ sender: Any) {
        self.user?.resendConfirmationCode()
            .continueWith(block: { (respone) -> Any? in
                let alert = UIAlertController(title: "Resent", message: "The confirmation code has been resent.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion:nil)
                return nil
            })
    }
    
}

