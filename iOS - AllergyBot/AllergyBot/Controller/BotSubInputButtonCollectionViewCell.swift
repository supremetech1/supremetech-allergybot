//
//  BotSubInputButtonCollectionViewCell.swift
//  AllergyBot
//
//  Created by Edwin chan on 14/03/2019.
//  Copyright © 2019 Edwin chan. All rights reserved.
//

import UIKit

class BotSubInputButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var botInputButton: UIButton!
    
    //Todo
    @IBAction func subInputButton(_ sender: UIButton) {

    }
    
    
    
    func displayContent(title: String) {
        botInputButton.setTitle(title, for: .normal)
        //        botInputButton.layer.cornerRadius = 12
    }
}
